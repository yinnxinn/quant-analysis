#!/usr/bin/env python
# coding: utf-8

# [曙光初现](!https://baike.baidu.com/item/%E6%9B%99%E5%85%89%E5%88%9D%E7%8E%B0/5197953?fr=aladdin)

# In[1]:



df = get_fundamentals(query(valuation))

# 曙光初现，次日开盘价低开5%以上，收盘价达到前日收盘价85%～95%
def judgeBottom(span):
    indexes = span.index 
    if (span.loc[indexes[0], 'close'] * 0.95 >= span.loc[indexes[1], 'open'] and  # 第二天大幅低开
        span.loc[indexes[1], 'close'] > span.loc[indexes[0], 'close'] and # 第二天收阳
        span.loc[indexes[0], 'close'] < span.loc[indexes[0], 'open'] and  # 第一天收阴
        span.loc[indexes[1], 'close'] < span.loc[indexes[0], 'open'] and  # 第二天收盘价落在第一天实体柱内
        span.loc[indexes[1], 'close'] > 0.5*(span.loc[indexes[0], 'open'] + span.loc[indexes[0], 'close']) # 大于50%
       ):
        return True
    return False


# 曙光初现，次日开盘价低开5%以上，收盘价达到前日收盘价85%～95%
def judgeBottom2(span):
    indexes = span.index
    if (span.loc[indexes[-2], 'close'] * 0.97 >= span.loc[indexes[-1], 'open'] and  # 第二天大幅低开3%以内
        span.loc[indexes[-1], 'close'] > span.loc[indexes[-2], 'close'] and  # 第二天收阳
        span.loc[indexes[-2], 'close'] < span.loc[indexes[-2], 'open'] and  # 第一天收阴
        span.loc[indexes[-1], 'close'] < span.loc[indexes[-2], 'open'] and  # 第二天收于第一天实体柱内
        span.loc[indexes[-1], 'close'] > 0.5*(span.loc[indexes[-2], 'open'] + span.loc[indexes[-2], 'close'])
        and span.loc[indexes[-1], 'open'] == 
        min([min([span.loc[indexes[i], 'close'], span.loc[indexes[i], 'open'], 
                  span.loc[indexes[i], 'high'], span.loc[indexes[i], 'low']]) 
             for i in range(0, len(indexes))]) # 15天最低价
       ):
        return True
    return False

# # 乌云盖顶
# def judgeTop(span):
#     indexes = span.index
#     if (span.loc[indexes[0], 'close'] * 0.95 >= span.loc[indexes[1], 'open'] and 
#         span.loc[indexes[1], 'close'] > span.loc[indexes[0], 'close'] and 
#         span.loc[indexes[0], 'close'] < span.loc[indexes[0], 'open'] and 
#         span.loc[indexes[1], 'close'] < span.loc[indexes[0], 'open'] and 
#         span.loc[indexes[1], 'close'] > 0.5*(span.loc[indexes[0], 'open'] + span.loc[indexes[0], 'close'])):
#         return True
#     return False


# In[ ]:


result = dict()

for index, row in df.iterrows():

    # 获取code过去2020～2021的行情
    prices = get_price(row['code'], start_date = '2020-01-01', end_date='2021-01-01')
    n = len(prices)

    result[str(row['code'])] = list()
    indexes = prices.index
    
    
    
    for i in range(n - 1):
        end = i+32 if i+32 < n else n-1
        end2 = i+92 if i+92 < n else n-1
        start = i+2 if i+1 < n else n-1
        tmp = dict()

        if judgeBottom(prices[i:i+2]):
            tmp['start'] = str(indexes[i+2]) #起始日期
            tmp['nextday'] = 1 if prices.loc[indexes[start], 'open'] > prices.loc[indexes[i+1], 'close'] else 0
            tmp['n30days'] = prices.loc[indexes[end], 'close']
            tmp['n90days'] = prices.loc[indexes[end2], 'close']
            tmp['profile'] = (prices.loc[indexes[end], 'close'] - prices.loc[indexes[start], 'open'])/prices.loc[indexes[start], 'close']
            tmp['profile2'] = (prices.loc[indexes[end2], 'close'] - prices.loc[indexes[start], 'open'])/prices.loc[indexes[start], 'close']
            result[str(row['code'])].append(tmp)
    print(' count ', index)


import json
with open('result_sgzx.json', 'w', encoding='utf-8') as w:
    w.write(json.dumps(result, ensure_ascii=False, indent=4))


# In[ ]:


get_ipython().run_line_magic('pinfo', 'attribute_history')


# In[2]:


result = dict()

for index, row in df.iterrows():

    # 获取code过去2020～2021的行情
    prices = get_price(row['code'], start_date = '2020-01-01', end_date='2021-01-01')
    n = len(prices)

    result[str(row['code'])] = list()
    indexes = prices.index
    
    
    
    for i in range(15,n - 1):
        end = i+30 if i+30 < n else n-1
        end2 = i+90 if i+90 < n else n-1
        start = i if i < n else n-1
        tmp = dict()

        if judgeBottom2(prices[i-15:i]):
            tmp['start'] = str(indexes[i]) #起始日期
            tmp['nextday'] = 1 if prices.loc[indexes[start], 'open'] > prices.loc[indexes[i+1], 'close'] else 0
            tmp['n30days'] = prices.loc[indexes[end], 'close']
            tmp['n90days'] = prices.loc[indexes[end2], 'close']
            tmp['profile'] = (prices.loc[indexes[end], 'close'] - prices.loc[indexes[start], 'open'])/prices.loc[indexes[start], 'open']
            tmp['profile2'] = (prices.loc[indexes[end2], 'close'] - prices.loc[indexes[start], 'open'])/prices.loc[indexes[start], 'open']
            result[str(row['code'])].append(tmp)
    print(' index ', index)


import json
with open('result_sgzx_min.json', 'w', encoding='utf-8') as w:
    w.write(json.dumps(result, ensure_ascii=False, indent=4))


# In[24]:


for index, row in df.iterrows():

    # 获取code过去2020～2021的行情
    prices = get_price(row['code'], start_date = '2020-01-01', end_date='2021-01-01')
    indexes = prices.index[0:10]
    span = prices[0:10]
     
    x =  [min([span.loc[indexes[i], 'close'], span.loc[indexes[i], 'open'], 
                  span.loc[indexes[i], 'high'], span.loc[indexes[i], 'low']]) 
             for i in range(0, len(indexes))]
    
    print(x, prices[0:10])
    import sys; sys.exit()


# In[25]:


import json
with open('result_sgzx_min.json', 'r', encoding='utf-8') as r:
    result = json.loads(r.read())


# In[26]:


## 次数统计
counts = dict()
for key, value in result.items():
    if value:
        try:
            counts[key] += 1
        except:
            counts[key] = 1
print(counts, len(counts), len(result))
print( len(counts)/len(result))        


# In[27]:


# 次日跌涨情况分析
import matplotlib.pyplot as plt
from collections import Counter
dz = list()
for key, value in result.items():
    
    if value:
        for item in value:
            dz.append(item['nextday'])
print(dz)

plt.rcParams['font.sans-serif']=['SimHei'] #用来正常显示中文标签

labels = ['上涨概率', '下跌概率']
print(dz, Counter(dz))
sizes = [sum(dz), len(dz)-sum(dz)]
explode = (0.1,0)
plt.pie(sizes,explode=explode,labels=labels,autopct='%1.1f%%',shadow=False,startangle=150)
plt.title('曙光初现次日涨跌分布')
plt.show()  


# In[33]:


# 收益率

margin = []
names = []
for key, value in result.items():
    if key == '603655.XSHG':
        continue
    for item in value:
        print(key, item)
        names.append(key)
        margin.append(item.get('profile2'))
print(len(margin), margin)


# In[35]:


plt.bar(range(len(margin)), margin)
plt.title("曙光初现次日买入持有90天的收益分布")
plt.show()


# In[32]:


## 计算平均收益
import numpy as np
print('平均收益', np.mean(margin))
print('最高收益', np.max(margin))
print('最低收益', np.min(margin))
print('中值收益', np.median(margin))
print('正收益期望', np.mean([item for item in margin if item>=0]))
print('负收益期望', np.mean([item for item in margin if item<0]))


# In[ ]:




